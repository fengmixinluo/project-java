package com.company;
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lib {
    public static int charnumber(File f) throws IOException {
        InputStreamReader read = new InputStreamReader(new FileInputStream(f), "utf-8");
        BufferedReader in;
        in = new BufferedReader(read);
        int Number_of_characters = 0;  //定义一个整型变量，用于统计字符数
        try {
            String str = null;
            while ((str = in.readLine()) != null) {  //readLine()方法, 用于读取一行,只要读取内容不为空就一直执行
                Number_of_characters += str.length();  //用于统计总字符数
            }
            //System.out.println("该文本共有" + Number_of_characters + "个字符");  //输出总的字符数
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Number_of_characters;
    }

    public static int wordsnumber(File f) throws IOException {
        InputStreamReader read = new InputStreamReader(new FileInputStream(f), "utf-8");
        BufferedReader in;
        in = new BufferedReader(read);
        StringBuffer cun = null;
        int wordnumber = 0;  //定义一个整型变量，用于统计单词数
        try {
            String temp = in.readLine();
            cun = new StringBuffer();
            while (temp != null) {
                cun.append(temp);
                cun.append(" ");  //每行结束多读一个空格
                temp = in.readLine();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String info = cun.toString();
        String regex = "[，。？,.? ]";
        String s[] = info.split(regex);
        /*统计单词个数*/
        for (int i = 0; i < s.length; i++) {
            if (s[i].length() >= 4) {
                String temp = s[i].substring(0, 4);
                temp = temp.replaceAll("[^a-zA-Z]", "");
                if (temp.length() >= 4) {
                    wordnumber++;
                }
            }
        }
        //System.out.println(wordnumber);
        return wordnumber;
    }

    public static int line(File f) throws IOException {
        InputStreamReader read = new InputStreamReader(new FileInputStream(f), "utf-8");
        BufferedReader in;
        in = new BufferedReader(read);
        int linenumber = 0;
        try {
            String lstr = null;
            while ((lstr = in.readLine()) != null) {
                linenumber++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println(linenumber);
        return linenumber;
    }

    public static  String Wordsfreqret(File f)throws IOException{
        InputStreamReader read = new InputStreamReader(new FileInputStream(f), "utf-8");
        BufferedReader in;
        in = new BufferedReader(read);
        String string = "";
        String temp_1="";
        Map<String, Integer> map = new HashMap<String, Integer>();
        try {
            while ((temp_1=in.readLine())!=null){
                string = string +temp_1;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        StringTokenizer st =new StringTokenizer(string);//分割
        String word_1;
        int count;
        while(st.hasMoreTokens()){
            word_1=st.nextToken(",?.!:\"\"' ' \n");
            if (map.containsKey(word_1)){
                count=map.get(word_1);
                map.put(word_1,count+1);
            }else {
                map.put(word_1,1);
            }
        }
        Comparator<Map.Entry<String, Integer>> valueComparator = new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue() - o1.getValue();
            }
        };
        // 输出结果
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(map.entrySet());
        Collections.sort(list, valueComparator);

        StringBuffer top10Words=new StringBuffer();

        for (int i=0;i<((10<=list.size())?10:list.size());i++) { //
            Map.Entry<String, Integer> entry = list.get(i);
//       System.out.println(entry.getKey() + ":" + entry.getValue());
            top10Words.append(entry.getKey() + ":" + entry.getValue()+"\n");
        }

        return  new String(top10Words);
    }

}
